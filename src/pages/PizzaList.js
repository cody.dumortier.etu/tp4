import Page from './Page';
import PizzaThumbnail from '../components/PizzaThumbnail';

export default class PizzaList extends Page {
	#pizzas;

	constructor(pizzas) {
		super('pizzaList'); // on pase juste la classe CSS souhaitée
		this.pizzas = pizzas;
	}

	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}

	update(data) {
		this.pizzas = data;
		this.element.innerHTML = this.render();
	}

	mount(element) {
		super.mount(element);
		const data = this.element.querySelector('.pizzaList');
		fetch('http://localhost:8080/api/v1/pizzas')
			.then(response => response.json())
			.then(data => this.update(data));
	}
}
